﻿using UnityEngine;

public class SpawnEffect : MonoBehaviour
{
    public GameObject pickUp;
    public GameObject evilPickUp;

    private SpawnerController spawner;

    public float spawnTime;

    private float randomPickUp;

    private void Start()
    {
        spawner = GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpawnerController>();
    }

    // Update is called once per frame
    private void Update()
    {
        spawnTime -= Time.deltaTime;
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 20);

        if (spawnTime <= 0)
        {
            Destroy(gameObject);

            randomPickUp = Random.Range(-9f, 9f);

            if (randomPickUp <= 0.5)
            {
                _ = Instantiate(pickUp, new Vector3(gameObject.transform.position.x, 0.5f, gameObject.transform.position.z), Quaternion.identity);
                spawner.IncreasePickUpValue();
            }
            else
            {
                _ = Instantiate(evilPickUp, new Vector3(gameObject.transform.position.x, 0.5f, gameObject.transform.position.z), Quaternion.identity);
            }
        }
    }
}
