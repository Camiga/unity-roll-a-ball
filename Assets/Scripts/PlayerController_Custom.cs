﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController_Custom : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;

    public Text countText;
    public Text highScore;
    public GameObject loseText;
    public GameObject pauseManager;
    private int count;

    private SpawnerController spawner;

    public AudioSource collectSound;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        spawner = GameObject.FindGameObjectWithTag("Respawn").GetComponent<SpawnerController>();
        loseText.SetActive(false);
        if (0 > PlayerPrefs.GetInt("highscore"))
        {
            PlayerPrefs.SetInt("highscore", 0);
        }
        highScore.text = "Highest Score: " + PlayerPrefs.GetInt("highscore");
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            spawner.LowerPickUpValue();
            Destroy(other.gameObject);
            count++;
            SetCountText();
            collectSound.Play();
        }

        if (other.gameObject.CompareTag("Evil Pick Up"))
        {
            pauseManager.SetActive(false);
            spawner.StopSpawning();
            gameObject.SetActive(false);
            loseText.SetActive(true);

            if (count > PlayerPrefs.GetInt("highscore"))
            {
                PlayerPrefs.SetInt("highscore", count);
            }
        }
    }

    private void SetCountText() => countText.text = "Current Score: " + count.ToString();
}
