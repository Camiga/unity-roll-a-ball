﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public void ResetTime() => Time.timeScale = 1;

    public void LoadMenu() => SceneManager.LoadScene("Main Menu");

    public void PlayDemo() => SceneManager.LoadScene("Demo");

    public void PlayCustom() => SceneManager.LoadScene("Custom");

    public void PlayEndless() => SceneManager.LoadScene("Endless");

    public void ResetHighest() => PlayerPrefs.SetInt("highscore", 0);

    public void QuitGame() => Application.Quit();
}
