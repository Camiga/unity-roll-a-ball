﻿using UnityEngine;

public class SunController : MonoBehaviour
{
    public GameObject pointLight;
    public float rotation;

    // Update is called once per frame
    private void Update()
    {
        if (rotation > 180)
        {
            pointLight.SetActive(true);
        }
        else
        {
            pointLight.SetActive(false);
        }

        if (rotation > 360)
        {
            rotation = 0;
        }

        rotation += 0.05f;
        Quaternion target = Quaternion.Euler(rotation, 45, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime);
    }
}
