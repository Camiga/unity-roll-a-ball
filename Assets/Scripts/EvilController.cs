﻿using UnityEngine;

public class EvilController : MonoBehaviour
{
    public float timeToKill;
    // Update is called once per frame
    private void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 10);

        timeToKill -= Time.deltaTime;
        if (timeToKill <= 0)
        {
            Destroy(gameObject);
        }
    }
}
