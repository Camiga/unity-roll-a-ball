﻿using UnityEngine;

public class PauseController : MonoBehaviour
{
    public GameObject pausePanel;
    private bool paused;

    // Start is called before the first frame update
    private void Start()
    {
        paused = false;
        pausePanel.SetActive(paused);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1 - Time.timeScale;
            pausePanel.SetActive(paused ^= true);
        }
    }
}
