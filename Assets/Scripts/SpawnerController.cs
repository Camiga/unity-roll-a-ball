﻿using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject spawnEffect;

    public float timeBetweenSpawns;
    public int maximumPickUps;

    private float localTimer;
    private int currentPickUps;

    // Update is called once per frame
    private void Update()
    {
        if (localTimer <= 0)
        {
            localTimer = timeBetweenSpawns;

            if (currentPickUps < maximumPickUps)
            {
                float randomX = Random.Range(-9f, 9f);
                float randomZ = Random.Range(-9f, 9f);

                _ = Instantiate(spawnEffect, new Vector3(randomX, 0.5f, randomZ), Quaternion.identity);
            }
        }
        else
        {
            localTimer -= Time.deltaTime;
        }
    }

    public void LowerPickUpValue() => currentPickUps--;

    public void IncreasePickUpValue() => currentPickUps++;

    public void StopSpawning() => currentPickUps += 100;
}
